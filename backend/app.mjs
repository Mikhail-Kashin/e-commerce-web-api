import express from 'express';
import dotenv from 'dotenv';
import morgan from 'morgan';
import mongoose from 'mongoose';
import cors from 'cors';
import authJWT from './helpers/jwt.mjs';
import errorHandler from './helpers/errorHandler.mjs';

import { router as productsRoutes } from './routes/products.mjs';
import { router as categoriesRoutes } from './routes/categories.mjs';
import { router as usersRoutes } from './routes/users.mjs';
import { router as ordersRoutes } from './routes/orders.mjs';

const app = express();

dotenv.config();

const port = 3000;
const api = process.env.API_URL;

app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());
app.options('*', cors());
app.use(authJWT());
app.use(errorHandler);

app.use(`${api}/products`, productsRoutes);
app.use(`${api}/categories`, categoriesRoutes);
app.use(`${api}/users`, usersRoutes);
app.use(`${api}/orders`, ordersRoutes);

mongoose.set('strictQuery', false);
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: 'eshop-database',
  })
  .then(() => {
    console.log('database connection established');
  })
  .catch(err => {
    console.log(err);
  });

app.listen(port, () => {
  console.log(`listening on ${port}`);
});
