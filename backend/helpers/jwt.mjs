import { expressjwt } from 'express-jwt';

const authJWT = () => {
  const api = process.env.API_URL;
  const secret = process.env.SECRET_KEY;
  return expressjwt({
    secret,
    algorithms: ['HS256'],
    isRevoked: isRevoked,
  }).unless({
    path: [
      { url: /\/public\/uploads(.*)/, methods: ['GET', 'OPTIONS'] },
      { url: /\/api\/v1\/products(.*)/, methods: ['GET', 'OPTIONS'] },
      { url: /\/api\/v1\/categories(.*)/, methods: ['GET', 'OPTIONS'] },
      { url: /\/api\/v1\/orders(.*)/, methods: ['GET', 'OPTIONS', 'POST'] },
      `${api}/users/login`,
      `${api}/users/register`,
    ],
  });
};

async function isRevoked(req, token) {
  if (!token.payload.isAdmin) {
    return true;
  }
}

export default authJWT;
